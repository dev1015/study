#-*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render #for detail
from django.http import HttpResponseRedirect, HttpResponse #form의 post위한 redirect
from django.core.urlresolvers import reverse #url 처리
from polls.models import Question, Choice

#컨트롤 함수정의
def index(request):
    #템플릿에 넘겨줄 객체생성
    latest_question_list = Question.objects.all().order_by('-pub_date')[:5]
    #템플릿에 넘겨줄 방식은 사전형{템플릿에서 사용될 변수명 : 해당하는 객체명}
    context = {'latest_question_list': latest_question_list }
    #템플릿 파일에 context변수를 적용하여 html생성, 이를 담아 httpResponse객체 반환
    return render(request, 'polls/index.html', context)

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})

def vote(request, question_id):
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': p,
            'error_message': "You didn't select a choice.",
            })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        #POST 데이터 정상처리시,
        #HttpResponseRedirect를 반환해 결과페이지로 리다이렉션
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question':question})
