#Main_program.py
# -*- coding: utf-8 -*-
import time
#Custom_module
import DB_Shooter
import DB_Connector
import sqlite_con
import excel_md
import html_md
import mail_md
f = open('header.txt', 'r')
header = f.readlines()
header_str = []
f.close
for idx, value in enumerate(header):
    header[idx] = header[idx].rstrip('\n')
    header_str.append(header[idx])
    header[idx] = header[idx].split(',')
db_data = DB_Connector.ConnectDB()
DB_Shooter.shootLOG(header[0],db_data,514) #Redmine to LOGSEE(header,data,port)
time.sleep(2) #db data save delay
query_data = sqlite_con.sqlite_connect('redmine',header_str[0]) #data for chief from sqlite
team_where = 'where stats_type <> "처리완료"' # where condition for team query
query_data_team = sqlite_con.sqlite_connect('redmine',header_str[2],team_where) #customize data for team
#DB_Shooter.shootLOG(header[2],query_data_team,515) #sqlite to logsee Shoot(team data)
time.sleep(2) #db data save delay
#-------------------------excel 모듈 응용부----------------------------------
e_data = excel_md.ExcelData() #출력에 필요한 기본 객체생성
e_data.setHeader(header[1]) #엑셀 상단 헤더파일
e_data.setQueryData(query_data) #쿼리결과리스트
e_data.createExcel('output_excel.xlsx') #출력파일
d_format_header = {'bold':True, 'bg_color' : 'C6EFCE', 'align':'center'} #헤더에 적용할 스타일설정
d_format_link = {'bold':True, 'color':'1F497D', 'underline':False} #링크 스타일
d_format_bg = {'bg_color': '99ccff'}
d_format_link_with_bg = {'bold':False, 'color':'blue', 'underline':1, 'bg_color':'99ccff'}
d_col_width = { 'C':6.25, 'F':90 } #해당하는 열 : 너비값
e_data.setFormat(d_format_header) #헤더 스타일 적용
e_data.setFormat(d_format_link) #링크 스타일 적용
e_data.setFormat(d_format_link_with_bg)
e_data.setFormat(d_format_bg) #행 조건에 맞는 배경색 지정
e_data.writeHeader() #헤더 출력
e_data.writeData() #엑셀 객체내 저장된 이중리스트형 데이터 출력(1:서식미적용, 2:서식적용)
e_data.setFilter() #헤더 스타일 적용
e_data.setColWidth(d_col_width)
e_data.workbook.close()
#------------------------------EOF:excel------------------------------------

#-------------------------------HTML Module---------------------------------
html_md.makeHTML('test.html',header[3],query_data_team,'이슈번호','제목')
#-------------------------------EOF:HTML------------------------------------

#---------------------------메일 발송 부분---------------------------------
f = open('test.html','r')
mail = f.read()
f.close()
i_mail = mail_md.MailSend()
i_mail.send_mail('ots830@netcruz.co.kr','ots830@netcruz.co.kr',['none CC'],'제목',mail, 'output_excel.xlsx')
