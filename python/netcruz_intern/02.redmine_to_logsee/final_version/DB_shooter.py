#DB_Shooter.py
# -*- coding:utf-8 -*-
from socket import socket, SOL_SOCKET, SOCK_DGRAM, SO_REUSEADDR, AF_INET

#로그 생성
def shootLOG(header,db_data,port):
    csock = socket(AF_INET, SOCK_DGRAM)
    csock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    csock.bind(('10.120.1.50', 30000))
    #쿼리결과(이중리스트) 탐색
    for idx, data in enumerate(db_data):
        if type(data) == tuple:
            print makeLog(data,header)
        else:
            print makeLog(data,header)
            break
    #로그전송
    #csock.sendto(log, ('10.120.1.50',port))

def makeLOG(data_row, header):
    log=''
    for idx, data in enumerate(data_row):
        #각 행속 요소들을 로그로 가공( header = "value", ... )
        if idx == (len(header)-1):
            #한 행의 끝일경우 , 없음
            log += header[idx] + "=" + '"' + str(data) + '"'
        else:
            log += header[idx] + "=" + '"' + str(data) + '"' + ','
    return log
