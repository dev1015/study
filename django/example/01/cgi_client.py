#!/usr/bin/env python
import urllib2
from urllib import urlencode

url = "http://localhost:5509/cgi-bin/script.py"
data = {
        'language' : 'python',
        'framework' : 'django',
        'email' : 'kim@naver.com',
        'tags' : 'hash tag #na noon o nul do noonmul el heulinda'
}
encData = urlencode(data)

f= urllib2.urlopen(url, encData)
print f.read()
