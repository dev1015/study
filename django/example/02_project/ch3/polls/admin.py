#-*- coding:utf-8 -*-
from django.contrib import admin
from polls.models import Question, Choice
# Register your models here.

#TabularInline - 표, StackedInline 리스트 같이보기
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 2

class QuestionAdmin(admin.ModelAdmin):
#필드 순서바꾸기
#    fields = ['pub_date', 'question_text']
#필드 분리해서 보여주기
    fieldsets = [
            ('Question Statement', {'fields': ['question_text']}),
            ('Date Information', {'fields': ['pub_date']}),
            ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['question_text']

admin.site.register(Question,QuestionAdmin)
admin.site.register(Choice)

