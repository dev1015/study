#--------------------------About----------------------------#

	# Project : Redmine Issue Report to LOGSEE
				==Refactoring(Redmine Project)
	# Version	: Final
	# Update	: 16.05.20
	# Developer : 오택승
	# Document Writer : 오택승

#-----------------------------HISTORY-----------------------#
	
	*Add (Data Dump, DataCompare Function)
	*Remove (Mail, Excel) Function from [Prototype Version]

#------------------------------------------------------------------------------------------------------------------------------------------------------------#
***Component***
#Main
 - Main_ver2-set.py

#OfficalModule
 - os				// need to command like'ls', delete file
 - pickle			// for dump DataQueryResult(LIST) to File
 - time			    // for sleep
 - datetime			// get DATE, TIME
#CustomModule
 - DB_Connector		// mysql DB Connect to Redmine(contain SQL Query)
 - DB_Shooter		// LOG(data=list struct) send to LOGSEE

#Required File
 - header			// Preset Header(Style Must be like csv!)

#Output
 - ./dump/DB_%Y-%M-%D_%H.%M.%S.dump  // QueryResultDump

***Operation Logic***
[Main]
: make Header List(from HeaderPreset)
: check Past Dumpfile
	@Dump file exist
		-> db_compare = True
	@Dump file does not exist
		-> db_compare = False
: connect RedmineDatabase(mysql)
	[DB_Connector.py]: return DBQueryResult

: DB_Compare with (PastDump, NewResult)
	@db_compare flag = TRUE
		-> using SET Struct get different DATA(Relative Complement)
		   (NewResult - PastDump) = contain(UpdateData,NewData)
	@db_compare flag = FALSE
		-> get ALL_DATA from DATABASE
	[DB_Shooter.py]: send Result(ALL or Relative) to LOGSEE

: dumping NEW DATA Result
	[pickle]: make Dump file to path(./dump/)

: delete past Dump File
