# tail.py
# -*- coding:utf-8 -*-
# CopyProgram Linux Command 'tail' [-n]option
# Develop by 오택승
# HowToUse : tail.py filename [-n][line number]
import sys
import time

def tail(line_num = 0):
    try:
        f = open(sys.argv[1], 'r')
    except:
        print '''>>Can\'t find file : %s ''' % sys.argv[1]
        sys.exit()
    else:
        #[-n:데이터 길이]로 슬라이싱하여 출력
        data = f.readlines()
        for idx, value in enumerate(data[-(line_num):len(data)]):
            print value,
        #메모리 절약을위해 로그배열 리스트 할당 해제
        del data
        #지속적 파일 모니터링, 새로운 내용 출력
        while (True):
            new_data = f.read()
            if len(new_data) != 0:
                print new_data,
            time.sleep(0.15)
#------------------------------------------------------#
#    Main
#------------------------------------------------------#
if __name__ == '__main__':
    #매개변수가 모두주어졌을시
    if len(sys.argv) > 2:
        try:
            sys.argv[2] == '-n'
            sys.argv[3] = int(sys.argv[3])
        except:
            print '>>please, input like $ tail \'file -n line_num\''
            print sys.argv[1]
        else:
           tail(sys.argv[3])
    #매개변수가 없을시
    elif len(sys.argv) == 1:
        print '>>please, insert file name! \n $ tail filename'
        sys.exit()
    #매개변수가 파일만 있을시
    elif len(sys.argv) == 2:
        tail()

