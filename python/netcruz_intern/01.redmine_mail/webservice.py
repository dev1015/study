#-*- coding:utf8 -*-
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
import subprocess
import urlparse

class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        #브라우저에서 페이지 get요청이외에 favicon을 요청하여
        #get 요청을 parsing해서 정상적인 인덱스에만 반응하도록함
        parsed_path = urlparse.urlparse(self.path)
        if parsed_path.path == '/favicon.ico':
            print parsed_path.path
        else:
            subprocess.Popen(['python','/opt/hts/ts/fin_redmine_mail.py'])
            self.wfile.write("""<head><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"></head>
        <h1>SMS Team</h1><h2>Redmine Report mail send Success</h>""")
        return

if __name__ == '__main__':
    server = HTTPServer(('', 5678), MyHandler)
    print "started WebServer on port 5678"
    print "press ^c quit server"
    server.serve_forever()
    print "end"

