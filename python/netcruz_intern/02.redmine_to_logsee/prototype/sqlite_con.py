# -*- coding: utf-8 -*-
import sqlite3
import datetime,time
import os
#db connection
def sqlite_connect(filter_type,q_header, where=''):
    #logsee DB Path Config
    s_date = datetime.date.today().isoformat()
    server_ip = '10.120.1.50'
    filter_type = 'redmine'
    db_dir_path = '/dev/shm/logsee/%s-%s/%s/' % (server_ip, filter_type, s_date)
    #listdir부를시 내림차순정렬으로 가장 최근파일이 첫리스트에 위치
    db_file_name = os.listdir(db_dir_path)[0]
    #ip,시간,필터명 기준으로 sqlite db를 가져올 path 설정
    #example) /dev/shm/logsee/아이피-필터명/날짜/시간-000.rt
    db_path = db_dir_path + db_file_name
    #sqld Query
    query = 'select %s from LOGS %s' % (q_header, where)
    #Query Execute
    con = sqlite3.connect(db_path)
    cursor = con.cursor()
    cursor.execute(query)
    db_data = cursor.fetchall()
    con.close()
    return db_data
