#Main_program.py
# -*- coding: utf-8 -*-
import os
import pickle
from datetime import datetime
#Custom_module
import DB_Shooter
import DB_Connector

#헤더생성
def makeHeader(textfile):
    f = open(textfile, 'r')
    header = f.readlines()
    f.close
    for idx, value in enumerate(header):
        header[idx] = header[idx].rstrip('\n')
        header[idx] = header[idx].split(',')
    return header

#메인
if __name__ == '__main__':
    db_comparison = False
    header = makeHeader('header.txt')

#가장최근 덤프된 파일확인
old_file = os.listdir('./dump/')
if len(old_file) == 0:
    db_comparison = False
else:
    db_comparison = True
    old_file.sort(reverse=True) #가장 최근 파일 가져오기
    old_path = './dump/%s' % old_file[0]
    print '@exist  :',old_path
    f = open(old_path,'r')
    old_db = set(pickle.load(f)) #이전 DB 가져오기
    f.close()

#Redmine DB연결
new_db = set(DB_Connector.ConnectDB())
#DB분석비교(최근더픔된 파일이 있을시)
if db_comparison == True:
    diff_db = new_db - old_db
    DB_Shooter.shootLOG(header[0], diff_db, 514)
else: #이전DB(덤프파일)이없을시(~최초실행시) 새로 가져온 DB내용 모두 전송
    DB_Shooter.shootLOG(header[0],new_db,514)
    print 'Warning! : Dump does not exist'
    print '@send DB : (ALL DATA)Complete'

#새로 읽어온 DB덤프
filename = datetime.today().strftime("%Y-%m-%d_%H.%M.%S")
dump_path = './dump/DB_%s.dump' % filename
print '@Created :', dump_path
f = open(dump_path,'w')
pickle.dump(new_db,f)
f.close()

#이전DB와 비교완료후에는 이전DB Dump를 삭제
if db_comparison == True:
    os.remove(old_path)
    print '@Remove : ',old_path
