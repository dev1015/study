#!/opt/hts/bin/python
#-*- coding:utf-8 -*-
import string
import xlsxwriter
import datetime
# 헤더파일 내용으로(utf-8인코딩) 리스트
class ExcelData:
    workbook = ''
    worksheet = ''
    header_col = ''
    style_format = []
    d_format_header = ''
    l_header = []   #헤더 리스트
    l_data = []     #데이터 리스트
    l_bg_row = [] #배경색이적용될 행 리스트
    def setHeaderFile(self, filename):        #csv(utf-8 인코딩)형식의 헤더목록파일을 이용해 헤더값 리스트 생성
        f_header = open(filename,'r')
        header_name = f_header.readline()
        f_header.close()
        header_name = header_name.split(u',')   #,로 분리된 리스트
        self.l_header = list((self.makeHeader(header_name), header_name))   #[칼럼주소(a1~z1),헤더값] 리스트생성
    #쿼리결과 데이터를 리스트로 저장(2중리스트로 구성되어있어야함)
    def setHeader(self, header):
        self.l_header = list((self.makeHeader(header),header))
    def setQueryData(self, queryData):
        self.l_data = queryData

    # 헤더 항목 갯수 조절 및 위치주소(a,b..) 생성
    def makeHeader(self, header_name):
        header_ends = len(header_name)
        header_col = list(string.uppercase[0:header_ends])
        # a1,b1같이 변환된 주소 리스트(header_col)
        for x in range(len(header_col)):
            header_col[x] = header_col[x]+'1'
            self.header_col = header_col
        return header_col

    #xlsxwirte모듈에서 쓸 엑셀 객체 생성
    def createExcel(self, filename):
        # Create a workbook and add a worksheet.
        self.workbook = xlsxwriter.Workbook(filename)
        self.worksheet = self.workbook.add_worksheet()

    # 스타일 포맷 설정
    def setFormat(self, dic):
        self.style_format.append(self.workbook.add_format(dic))

    # 헤더에 데이터 쓰기(헤더주소, 헤더값, 스타일포맷)
    def writeHeader(self):
        for x in range(len(self.l_header[0])):
            self.worksheet.write(self.l_header[0][x], self.l_header[1][x], self.style_format[0])

    # 헤더에 필터 적용
    def setFilter(self):
        self.worksheet.autofilter(self.l_header[0][0] + ':' + self.l_header[0][-1])

    # 열 너비 지정
    def setColWidth(self, d_col):
        for x in range(len(d_col)):
            self.worksheet.set_column(d_col.keys()[x] + ':' + d_col.keys()[x],
                                      d_col.get(d_col.keys()[x]))

    # 데이터 작성(이중리스트)
    def writeData(self):
            #행열번호를 활용해 기록시
            s_link = 'http://redmine.netcruz.co.kr/redmine/issues/'
            for x in range(len(self.l_data)):
                form_style = self.style_format[1]
                for y in range(len(self.l_data[x])):
                    if (y == self.l_header[1].index('이슈번호')): #링크정보가 들어갈셀(현재 0번째가 이슈넘버,4번째가 제목? / 링크를달것임)
                        s_link_num = s_link + self.l_data[x][y]
                        #링크정보기록
                        self.worksheet.write_url(x+1, y, s_link_num)
                        #링크가들어갈 셀에 내용 기록(스타일포맷 링크정보기록과 셀내용에 포맷 일치해야함)
                        #현재 이슈넘버를 숫자형으로 기록해 write_number사용하였으므로 적절히 조정
                        self.worksheet.write_number(x+1, y, int(self.l_data[x][y]), form_style)
                    elif (y == self.l_header[1].index('제목')): #제목일경우 이슈넘버와 동일한 링크제공
                        self.worksheet.write_url(x+1, y, s_link_num)
                        self.worksheet.write(x+1, y, self.l_data[x][y], form_style)
                    else:
                        self.worksheet.write(x+1, y, self.l_data[x][y])

    # 변경일 2주이내 배경색 하늘색
    def bgDraw(self, criteria, date_diff):
        date_today = datetime.date.today()
        for idx_1, data in enumerate(self.l_data):
            temp = data[criteria][:10].split(u'-')
            for idx_2, data2 in enumerate(temp):
                temp[idx_2] = int(data2)
            data_day = datetime.date(temp[0],temp[1],temp[2]) #날짜형으로 완성
            if (date_today - data_day) < datetime.timedelta(date_diff): #날짜가 14일 이내인가 비교
                #print data_day, type(data_day), date_today, idx_1, len(data)
                self.worksheet.set_row(idx_1+1, None,self.style_format[2])
                self.l_bg_row.append(idx_1)    #조건에 성립하는 행에대한 리스트

