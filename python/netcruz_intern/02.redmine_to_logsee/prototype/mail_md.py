import smtplib
import os
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import Encoders
from email import utils
from email.header import Header

class MailSend():
    def __init__(self,smtp_server="smtp.gmail.com",port=587,userid='ots830@netcruz.co.kr',passwd='pika5509'):
        self.smtp_server = smtp_server
        self.port = port
        self.userid = userid
        self.passwd = passwd

    def send_mail(self,from_user, to_user, cc_users, subject, text, attach):
            COMMASPACE = ", "
            msg = MIMEMultipart()
            msg["From"] = from_user
            msg["To"] = to_user
            msg["Subject"] = Header(s=subject, charset="utf-8")
            msg["Date"] = utils.formatdate(localtime = 1)
            msg.attach(MIMEText(text, "html", _charset="utf-8"))

            if (attach != None):
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(open(attach, "rb").read())
                    Encoders.encode_base64(part)
                    part.add_header("Content-Disposition", "attachment; filename=\"%s\"" % os.path.basename(attach))
                    msg.attach(part)

            smtp = smtplib.SMTP(self.smtp_server, self.port)
            smtp.ehlo()
            smtp.starttls()
            smtp.ehlo()
            smtp.login(self.userid, self.passwd)
            smtp.sendmail(from_user, to_user, msg.as_string())
            smtp.close()
