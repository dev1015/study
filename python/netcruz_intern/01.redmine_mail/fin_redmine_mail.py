#-*- coding:utf8 -*-
#   같은 경로에 필요파일
#   html_sample.html    <-  html 틀을위한 기본 테이블 .html 파일
#   sms_member.txt  <-  SMS 팀원의 메일주소 관리를 위한 메일 정보  .txt 파일

import os
import sys
import time
import codecs
import string
import smtplib
import MySQLdb
import xlsxwriter
import datetime as dttm

from email import Encoders
from email.header import Header
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart

#헤더파일 내용으로(utf-8인코딩) 리스트
class ExcelData:
    f_header = ''
    style_format = []
    d_format_header = ''
    l_header = []    #헤더 리스트
    l_data = []     #데이터 리스트

    def setHeaderFile(self, header_list):        #csv(utf-8 인코딩)형식의 헤더목록파일을 이용해 헤더값 리스트 생성
        self.l_header = [self.makeHeader(header_list), header_list]   #[칼럼주소(a1~z1),헤더값] 리스트생성

    def setQueryData(self, queryData):  #쿼리결과 데이터를 리스트로 저장(2중리스트로 구성되어있어야함)
        for i in queryData: #레드마인 데이터 리스트 생성
            temp = []
            for x in i:
                temp.append(str(x))
            self.l_data.append(temp)

    # 헤더 항목 갯수 조절 및 위치주소(a,b..) 생성
    def makeHeader(self, header_name):
        header_ends = len(header_name)
        header_col = list(string.uppercase[0:header_ends])
        # a1,b1같이 변환된 주소 리스트(header_col)
        for x in range(len(header_col)):
            header_col[x] = header_col[x]+'1'
        return header_col

    def writeData(self):
        s_link = 'http://redmine.netcruz.co.kr/redmine/issues/'
        #행열번호를 활용해 기록시
        s_link = 'http://redmine.netcruz.co.kr/redmine/issues/'
        for x in range(len(self.l_data)):
            form_style = self.style_format[1]
            for y in range(len(self.l_data[x])):
                if (y == 2): #링크정보가 들어갈셀(현재 0번째가 이슈넘버,4번째가 제목? / 링크를달것임)
                    s_link_num = s_link + self.l_data[x][y]
                    #링크정보기록
                    self.worksheet.write_url(x+1, y, s_link_num)
                    #현재 이슈넘버를 숫자형으로 기록해 write_number사용하였으므로 적절히 조정
                    self.worksheet.write_number(x+1, y, int(self.l_data[x][y]), form_style)
                elif (y == 7): #제목일경우 이슈넘버와 동일한 링크제공
                    self.worksheet.write_url(x+1, y, s_link_num)
                    self.worksheet.write(x+1, y, self.l_data[x][y], form_style)
                else:
                    self.worksheet.write(x+1, y, self.l_data[x][y])

    #xlsxwirte모듈에서 쓸 엑셀 객체 생성
    def createExcel(self, filename):
        # Create a workbook and add a worksheet.
        self.workbook = xlsxwriter.Workbook(filename)
        self.worksheet = self.workbook.add_worksheet()

    # 스타일 포맷 설정
    def setFormat(self, dic):
        self.style_format.append(self.workbook.add_format(dic))

    # 헤더에 데이터 쓰기(헤더주소, 헤더값, 스타일포맷)
    def writeHeader(self):
        for x in range(len(self.l_header[0])):
            self.worksheet.write(self.l_header[0][x], self.l_header[1][x], self.style_format[0])

    # 헤더에 필터 적용
    def setFilter(self):
        self.worksheet.autofilter(self.l_header[0][0] + ':' + self.l_header[0][-1])

    # 열 너비 지정
    def setColWidth(self, d_col):
        for x in range(len(d_col)):
            self.worksheet.set_column(d_col.keys()[x] + ':' + d_col.keys()[x],
                                      d_col.get(d_col.keys()[x]))



now = dttm.datetime.now()
str_now = now.strftime("%Y-%m-%d")
today = dttm.datetime.strptime(str_now,"%Y-%m-%d")
user = "ots830@netcruz.co.kr"   #송신자 메일 주소
pwd = "pika5509"
files = "/opt/hts/netcruz/cronjobs/logsee"   #해당경로
search_file = "Redmine Issues("+str_now+").xlsx" #첨부파일명
file_pwd = os.path.join(files,search_file)  #해당경로에 첨부파일명을 검색해서 파일의 경로를 찾음
files_pwd = [os.path.join(files,search_file) for search_file in os.listdir(files)]  #파일들의 경로
file_split = file_pwd.split("/opt/hts/netcruz/cronjobs/logsee/")  #파일명을 제외한 나머지 경로를 기준으로 나눔
file_name = file_split[1]   #첨부파일 데이터

def send_mail(to,subject,html,attach):
    sms_special_id = ["ots830@netcruz.co.kr"]
    msg = MIMEMultipart()
    msg["Subject"] = Header(subject.encode('utf-8'),'UTF-8').encode()
    msg["From"] = Header(user.encode('utf-8'),'UTF-8').encode()
    msg["To"] = Header(to.encode('utf-8'),'UTF-8').encode()
    msg.attach(MIMEText(html.encode('utf-8'),'html','UTF-8'))

    for id in sms_special_id:
        if (to == id):  #멤버중 팀장님에게만 .csv 파일 을 추가

            file = MIMEBase('application','octec-stream')
            file.set_payload(open(file_name,'rb').read())  #페이로드 설정
            Encoders.encode_base64(file)
            file.add_header('Content-Disposition','attachment; filename="%s"'%os.path.basename(file_name))
            msg.attach(file)

    try:
        mailServer = smtplib.SMTP("smtp.gmail.com",587)
        mailServer.ehlo()
        mailServer.starttls()
        mailServer.login(user,pwd)
        mailServer.sendmail(user,to,msg.as_string())
        mailServer.close()

    except:
        print 'Mail Setting Error : https://support.google.com/mail/answer/14257?rd=1'
        #구글에서 SMTP 보안 관련 개인 설정 필요 : 송신자의 메일 앱 보안수준이
        #더 낮은 앱이 엑세스 할수 있도록 설정 요망

def db_Connect(query):
    db = MySQLdb.connect("10.120.1.253","bitnami","488f050f8a","bitnami_redmine")
    db.query("set character_set_connection = utf8;")
    db.query("set character_set_server = utf8;")
    db.query("set character_set_client = utf8;")
    db.query("set character_set_results = utf8;")
    db.query("set character_set_database = utf8;")
    cursor = db.cursor()
    cursor.execute(query.encode('utf8'))
    data = cursor.fetchall()
    final_data = list(data)
    db.close()
    return final_data

def list_throw(total_list,throw_index): #총 데이터리스트에서 한 행씩만 뽑아던져줌
    return total_list[throw_index]

def make_gap_day(team_list,index):
    day_str = team_list[index]

    if index ==3:
        day = dttm.datetime.strptime(day_str,"%Y-%m-%d")
    else:
        day_split = day_str.split(" ")
        day = dttm.datetime.strptime(day_split[0],"%Y-%m-%d")

    gap_day = today - day
    gap_str = str(gap_day)
    gap_split = gap_str.split(" ")
    gap = gap_split[0]
    return gap

def make_html(team_data,id_dic):    #기본 HTML 샘플에 데이터를 덮어씌우는 알고리즘 함수
    html_sample_list = []
    team_data_list = []
    index = 0
    throw_index = 0
    data_num=0
    cnt = 0
    f = open("/opt/hts/netcruz/cronjobs/logsee/html_sample.html",'r')
    lines = f.readlines()
    for line in lines:   #샘플리스트 생성
        html_sample_list.append(line)
    f.close()

    for i in team_data: #레드마인 데이터 리스트 생성
        temp = []
        for x in i:
            temp.append(str(x))
            data_num+=1
        team_data_list.append(temp)
    s = data_num/10
    start = 17 + (s*22)
    end = len(html_sample_list)-2

    for i in range(start,end):
        del html_sample_list[start]
        end = len(html_sample_list)-2
        if start > end:
            break

    for hindex,tag in enumerate(html_sample_list):

        s1 = '<tr bgcolor = "#F2F2F2">'
        s2 = '<tr bgcolor = "white">'
        if tag == '    <tr>\n':

            if not cnt % 2:

                html_sample_list[hindex] = s1
                cnt += 1

            else:

                html_sample_list[hindex] = s2
                cnt += 1
        else:
            continue
    for hindex,tag in enumerate(html_sample_list):
        if tag == '        <td>\n':
            if index == 10:
                index = 0
                throw_index += 1
            team_list = list_throw(team_data_list,throw_index)

            if index == 0 or index == 8:
                issue_num = team_list[0]
                html_sample_list[hindex] = '<td><a href="http://redmine.netcruz.co.kr/redmine/issues/'+issue_num+'"style="text-decoration:none;color:#0040FF">'+team_list[index]+"</a>"
                index += 1

            elif index  == 1 or index == 2:
                for k in id_dic.keys():
                    if k == team_list[index] :
                        id_value = id_dic.get(k)
                        id = str(id_value)
                        html_sample_list[hindex] = '<td><a href="http://redmine.netcruz.co.kr/redmine/users/'+id+'" style="text-decoration:none;color:#0040FF">'+team_list[index]+"</a>"
                index += 1

            elif index == 3:
                issue_gap_day = make_gap_day(team_list,3)
                update_gap_day = make_gap_day(team_list,4)

                if issue_gap_day == '0:00:00' or update_gap_day == '0:00:00':
                    html_sample_list[hindex] = '<td>'+team_list[index]
                    index += 1
                    continue

                issue_gap = int(issue_gap_day)
                update_gap = int(update_gap_day)

                if issue_gap >= 90 or update_gap <= 14:
                    html_sample_list[hindex] = '<td bgcolor = "#F4FA58">'+team_list[index]
                else:
                    html_sample_list[hindex] = '<td>'+team_list[index]
                index += 1

            elif index == 4:
                update_day_full = team_list[index]
                update_day_split = update_day_full.split(" ")
                html_sample_list[hindex] = '<td>'+update_day_split[0]
                index += 1

            else:
                html_sample_list[hindex] = '<td>'+team_list[index]
                index += 1

        else:
            continue

    f = open("/opt/hts/netcruz/cronjobs/logsee/RedMine_Data/html.html",'w')
    for line in html_sample_list:
        f.writelines(line)
    f.close()

    f = open("/opt/hts/netcruz/cronjobs/logsee/RedMine_Data/html.html",'r')
    read = f.read()
    f.close
    return read

def main():
    title = " Redmine Issues ("+str_now+") "
#------------------------------< 팀원들에게 보내는 Issue Query >-------------------------------------
    team_query = """SELECT  Subquery.```#```,   #이슈번호
        Subquery.assigned_name, #담당자
        Subquery.author_name,   #보고자
        Subquery.start_date,    #이슈발생일자
        Subquery.updated_on,    #최근변경일자
        Subquery_2.result,      #처리내용
        Subquery.tracker_name,  #유형
        Subquery.status_name,   #상태
        Subquery.subject,       #제목
        Subquery_1.request      #요청내용

    FROM ((SELECT issues.id AS ```#```,
                trackers.name AS tracker_name,
                issue_statuses.name AS status_name,
                issues.subject,
                concat(users_for_author.lastname, users_for_author.firstname)
                   AS author_name,
                issues.start_date,
                concat(users_for_assigned.lastname,
                       users_for_assigned.firstname)
                   AS assigned_name,
                issues.due_date,
                issues.updated_on,
                issues.done_ratio
           FROM (((bitnami_redmine.trackers trackers
                   INNER JOIN bitnami_redmine.issues issues
                      ON (trackers.id = issues.tracker_id))
                  INNER JOIN bitnami_redmine.issue_statuses issue_statuses
                     ON (issue_statuses.id = issues.status_id))
                 INNER JOIN bitnami_redmine.users users_for_author
                    ON (users_for_author.id = issues.author_id))
                INNER JOIN bitnami_redmine.users users_for_assigned
                   ON (users_for_assigned.id = issues.assigned_to_id)
          WHERE (    issues.project_id IN (24, 48,
                                           52,
                                           55,
                                           56)
                 AND issues.closed_on IS NULL and issue_statuses.name<>"처리완료")) Subquery
        INNER JOIN
        (SELECT custom_values.customized_id, custom_values.value AS result
           FROM bitnami_redmine.custom_values custom_values
                INNER JOIN bitnami_redmine.issues issues
                   ON (custom_values.customized_id = issues.id)
          WHERE (    custom_values.custom_field_id = 30
                 AND (    issues.project_id IN (24, 48,
                                                52,
                                                55,
                                                56)
                      AND issues.closed_on IS NULL))) Subquery_2
           ON (Subquery.```#``` = Subquery_2.customized_id))
       INNER JOIN
       (SELECT custom_values.customized_id, custom_values.value AS request
          FROM bitnami_redmine.custom_values custom_values
               INNER JOIN bitnami_redmine.issues issues
                  ON (custom_values.customized_id = issues.id)
         WHERE (    custom_values.custom_field_id = 3
                AND (    issues.project_id IN (24, 48,
                                               52,
                                               55,
                                               56)
                     AND issues.closed_on IS NULL))) Subquery_1
          ON (Subquery.```#``` = Subquery_1.customized_id)
ORDER BY Subquery.assigned_name asc, Subquery.updated_on desc """
#----------------------------------------------------------------------------------------------------


#------------------------------< 팀장님께 보내는 Issue Query >---------------------------------------
    cheif_query = """SELECT  concat(USER_ASSIGNED.lastname, USER_ASSIGNED.firstname) as Assigned,   #담당자
        ISS.start_date as StartDate,         #이슈발생일
        ISS.id,                  #이슈넘버
      PRJ.PrjType,            #업무유형
        TRKR.TrkType,            #유형
        STATS.StatsType,         #상태
        PRIO.name as Priority,      #우선순위
        ISS.subject,            #제목
        concat(USER_AUTHOR.lastname, USER_AUTHOR.firstname) as Author,   #보고자
        IC.IssueCheckName,                     #이슈확인자
        ISS.created_on as CreateDate,         #등록일
        ISS.updated_on as UpdateDate,         #변경일
        ISS.due_date as DoneDate,            #처리완료일
      PROCS.ProcsDate,         #처리예정시한
      ESTD.EstDate,            #적용예정시한
        PROCSRQST.ProcsRequest,      #요청내용
      PROCSRST.ProcsResult,      #처리내용
        CS.Customer,            #고객사
        ASTD.Standard,            #표준본적용
        SL.Solution,            #솔루션
        PRJ_VERSION.name as ProjectName,      #프로젝트
        ISS_CATEGORY.name as Cateogry         #카테고리
FROM bitnami_redmine.issues as ISS
   #--숫자값으로 대체된 값에 대한 치환 / 필수값은 INNER JOIN , 빈값이있을수있는값은 LEFT OUTER JOIN
       INNER JOIN #업무유형 / 셀랙 안하고 불러올시 0.3초 느림
         (SELECT id, name as PrjType
            from bitnami_redmine.projects) as PRJ
         ON ISS.project_id = PRJ.id

      INNER JOIN #유형
         (SELECT id, name as TrkType
            from bitnami_redmine.trackers) as TRKR
         ON ISS.tracker_id = TRKR.id

        INNER JOIN #상태
         (SELECT id, name as StatsType
            from bitnami_redmine.issue_statuses) as STATS
         ON ISS.status_id = STATS.id

        INNER JOIN #우선순위
         bitnami_redmine.enumerations as PRIO
         ON ISS.priority_id = PRIO.id

        INNER JOIN #보고자
         bitnami_redmine.users as USER_AUTHOR
         ON ISS.author_id = USER_AUTHOR.id

        LEFT OUTER JOIN #담당자
         bitnami_redmine.users as USER_ASSIGNED
         ON ISS.assigned_to_id = USER_ASSIGNED.id

        LEFT OUTER JOIN #프로젝트
         bitnami_redmine.versions as PRJ_VERSION
         ON ISS.fixed_version_id = PRJ_VERSION.id
      LEFT OUTER JOIN #범주
         bitnami_redmine.issue_categories as ISS_CATEGORY
         ON ISS.category_id = ISS_CATEGORY.id
   #--기본 issue테이블에 존재하지않고 custom_values테이블에 저장된값들을 불러오기위한
    #--OuterJoin '값이 null이거나 해당 커스텀필드가 없는 이슈도있기때문에 outer로')
       LEFT OUTER JOIN #요청내용
         (SELECT custom_values.customized_id AS ID, custom_values.value AS ProcsRequest
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 3) AS PROCSRQST
           ON (ISS.id = PROCSRQST.id)

       LEFT OUTER JOIN #처리내용
         (SELECT custom_values.customized_id AS ID, custom_values.value AS ProcsResult
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 30) AS PROCSRST
           ON (ISS.id = PROCSRST.id)

       LEFT OUTER JOIN #처리예정시한
         (SELECT custom_values.customized_id AS ID, custom_values.value AS ProcsDate
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 24) AS PROCS
           ON (ISS.id = PROCS.id)

       LEFT OUTER JOIN #적용예정시한
         (SELECT custom_values.customized_id AS ID, custom_values.value AS EstDate
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 25) AS ESTD
           ON (ISS.id = ESTD.id)

       LEFT OUTER JOIN #고객사
         (SELECT custom_values.customized_id AS ID, custom_values.value AS Customer
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 9) AS CS
           ON (ISS.id = CS.id)

       LEFT OUTER JOIN #이슈확인자
         (SELECT custom_values.customized_id AS ID, custom_values.value AS IssueCheck, concat(USER_ASSIGNED.lastname, USER_ASSIGNED.firstname) as IssueCheckName
            FROM bitnami_redmine.custom_values AS custom_values
            left outer JOIN #담당자
            bitnami_redmine.users as USER_ASSIGNED
            ON custom_values.value = USER_ASSIGNED.id
            WHERE custom_values.custom_field_id = 45) AS IC
           ON (ISS.id = IC.id)

       LEFT OUTER JOIN #표준본적용여부
         (SELECT custom_values.customized_id AS ID, custom_values.value AS Solution
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 29) AS SL
           ON (ISS.id = SL.id)

       LEFT OUTER JOIN #솔루션 적용여부
         (SELECT custom_values.customized_id AS ID, custom_values.value AS Standard
           FROM bitnami_redmine.custom_values custom_values
          WHERE custom_values.custom_field_id = 46) AS ASTD
           ON (ISS.id = ASTD.id)


 WHERE #--해당이슈의 project_id를 체크하여 우리팀 이슈인지 확인)
       (ISS.project_id IN
          #--서브쿼리 : 우리팀 이슈를 판단할 ProjectID 가져오기 --
           (SELECT bitnami_redmine.projects.id
              FROM bitnami_redmine.projects
             WHERE    (bitnami_redmine.projects.parent_id = 48)
                   OR                                              #2016 nLM하위 메뉴들
                     (bitnami_redmine.projects.id = 48)
                   OR                                                #2016*(nLM자체)
                     (bitnami_redmine.projects.id = 24))            #2015(SMS팀)
         #--eof 서브쿼리--
        )  #완료된 이슈는 제외
order by Assigned asc, ISS.id desc; """
#----------------------------------------------------------------------------------------------------

#------------------------------------< 담당자 이름,ID Query >----------------------------------------
    getid_query = """ SELECT concat(lastname,firstname) as name, id FROM bitnami_redmine.users; """
#----------------------------------------------------------------------------------------------------

    team_data = db_Connect(team_query)
    cheif_data = db_Connect(cheif_query)
    id_data = db_Connect(getid_query)
    id_dic = dict(id_data)
    final_html_list = make_html(team_data,id_dic)
    header_list = ["담당자","이슈발생일자","#","업무유형","유형","상태","우선순위","제목","보고자","이슈확인자","등록","변경","처리완료일자","처리예정시한","적용예정시한","요청내용","처리내용","고객사","표준본적용여부","솔루션","프로젝트","범주"]

    #** Excel 파일 생성 **
    e_data = ExcelData() #출력에 필요한 기본 객체생성
    e_data.setHeaderFile(header_list) #헤더파일
    e_data.setQueryData(cheif_data) #쿼리결과를 가져올시 setQueryData(쿼리결과리스트)
    e_data.createExcel("/opt/hts/netcruz/cronjobs/logsee/RedMine_Data/Redmine Issues("+str_now+").xlsx") #출력파일 생성
    d_format_header = {'bold':True, 'bg_color' : 'C6EFCE', 'align':'center'} #헤더에 적용할 스타일설정
    d_format_link = {'bold':True, 'color':'1F497D', 'underline':False} #링크 스타일
    d_col_width = { 'A':6.25, 'H':90 } #해당하는 열 : 너비값
    e_data.setFormat(d_format_header) #헤더 스타일 적용
    e_data.setFormat(d_format_link) #링크 스타일 적용
    e_data.writeHeader() #헤더 출력
    e_data.writeData() #엑셀 객체내 저장된 이중리스트형 데이터 출력
    e_data.setFilter() #헤더 스타일 적용
    e_data.setColWidth(d_col_width)
    e_data.workbook.close()


    f = open("/opt/hts/netcruz/cronjobs/logsee/sms_member.txt","r")
    members = f.readlines()
    for line in members:
        member_line = line.split('\n')
        member = member_line[0]
        send_mail(member,title,final_html_list,files_pwd)
        time.sleep(3)   #메일 한통을 보내는데 프로세스 처리시간을 고려해 3초 간격으로 메일을 보냄
    f.close()

if __name__ == '__main__':
    main()
