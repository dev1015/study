#--------------------------About----------------------------#

	# Project : Redmine_Issue_ReportSystem_proto(ver.Logsee)
				==Refactoring(Redmine Project)
	# Version	: Prototype
	# Update	: 15.05.17
	# Developer : 오택승
	# Document Writer : 오택승

#------------------------------------------------------------------------------------------------------------------------------------------------------------#

***Component***
#Main
 - Main_program.py  // db connect and process data,

#OfficalModule
 - time			    // for sleep

#CustomModule
 - DB_Connector		// mysql DB Connect to Redmine(contain SQL Query)
 - DB_Shooter		// LOG(data=list struct) send to LOGSEE
 - sqlite_con		// Connect to LOGSEE LocalDB
 - mail_md			// Send Mail(Content:html, attach:xlsx or none)
 - html_md			// Make HTML(with Table)
 - excel_md			// make xls(excel) file(using 'xlsxwrite'module)

#Required File
 - header.txt		// Preset Header(style like csv)

#Output
 - output_excel.xlsx  // made by excel_md include DBdata

***Logic***
 -> Read preset HeaderFile
 -> DB_Connect to Redmine(Mysql)	//DB_Connector.py
 -> DB Data send to LOGSEE			//DB_Shooter.py
 -> Connect LOG to LOGSEE LocalDB	//sqlite_con.py
 -> Query(alldata, some)
	([list(tuple)] struct)
	 -> LocalDB convert to data		//sqlite_con.py
 -> make html for mail // html_md.py
	and excel(xlsx)file // excel_md.py
 -> send mail html, and attach output excelfile for someone
