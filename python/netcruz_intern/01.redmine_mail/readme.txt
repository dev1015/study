#--------------------------About----------------------------#
	
	# Project : Redmine_Issue_Report
	# Developer : 오택승(excel custom module and dummy logic)
			            황용하(main, create html and mail logic)
	# Update	: 15.05.09
	# Document Writer : 오택승
	
#------------------------------------------------------------------------------------------------------------------------------------------------------------#

***Component***
#Main
 - fin_redmine_mail.py // db connect and process data,
					  // make html, send mail, make xls(use custom module)

#CustomModule
 - excel_md			  // make xls(excel) file(using 'xlsxwrite'module)

#RequireFile
 - header.csv		  // list of header_value for write table(included in mail) and xls firstROW(header)
 - sms_member.txt	  // address list for sending mail
 - html_sample.html   // define table structure for making html

#Output
 - output_excel.xlsx  // made by excel_md include DBdata


***Logic***
redmine database(mysql) or read dummydata.csv
 -> query request(or read dummy)
 -> result convert to data([list(tuple)] struct)
 -> make html for mail // fin_redmine_mail.py 
	and excel(xlsx)file // excel_md.py
 -> send mail html, and attach output excelfile for someone


***External service***
 # webservice.py //Simple HTTP Server remotely run fin_redmine_mail.py
 ->run webservice.py
 ->connect localhost(your ip):5678
 ->output simple html text while running fin_redmine_mail.py
