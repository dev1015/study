#DB_Shooter.py
# -*- coding:utf-8 -*-
from socket import socket, SOL_SOCKET, SOCK_DGRAM, SO_REUSEADDR, AF_INET

#로그 생성(1차원 리스트나 튜플을 헤더와 결합해 로그가공)
def makeLOG(l_data_row, header):
    log=''
    for idx, data in enumerate(l_data_row):
        #각 행속 요소들을 로그로 가공( header = "value", ... )
        if idx == (len(header)-1):
            #한 행의 끝일경우 , 없음
            log += header[idx] + "=" + '"' + str(data) + '"'
        else:
            log += header[idx] + "=" + '"' + str(data) + '"' + ','
    return log
def shootLOG(header,db_data,target_ip='10.120.1.50',port=514):
    csock = socket(AF_INET, SOCK_DGRAM)
    csock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    csock.bind(('10.120.1.50', 30000))
    #데이터 탐색
    #데이터가 DB 한개 튜플인가 아니면, 튜플이모인 리스트인가판단
    #이전 Dump가 없는상황에서는 데이터쿼리결과 리스트튜플을 전달받음
    for idx, data in enumerate(db_data):
        #전체DB튜플(튜플형리스트)를 넘겨받았을시 리스트내 한 튜플씩 전달
        if type(data) in (tuple,list,set):
            csock.sendto(makeLOG(data,header), ('10.120.1.50',port))
            print '#send :',makeLOG(data,header)
        else:
        #만약 한개의 튜플만 전달받을시 해당 튜플로 로그가공(set미적용로직)
            csock.sendto(makeLOG(db_data,header), ('10.120.1.50',port))
            print '#one :',makeLOG(db_data,header)
            break
