#DB_connector.py
# -*- coding:utf-8 -*-
import MySQLdb

c_query = '''
SELECT  ISS.id,                     #이슈넘버
        PRJ.PrjType,                #업무유형
        TRKR.TrkType,               #유형
        STATS.StatsType,            #상태
        PRIO.name as Priority,      #우선순위
        ISS.subject,                #제목
        concat(USER_AUTHOR.lastname, USER_AUTHOR.firstname) as Author,  #보고자
        concat(USER_ASSIGNED.lastname, USER_ASSIGNED.firstname) as Assigned,    #담당자
        IC.name as CheckName,                            #이슈확인자
        ISS.start_date as StartDate,            #이슈발생일
        ISS.created_on as CreateDate,           #등록일
        ISS.updated_on as UpdateDate,           #변경일
        ISS.due_date as DoneDate,               #처리완료일
        PROCS.ProcsDate,            #처리예정시한
        ESTD.EstDate,               #적용예정시한
        #--ISS.done_ratio,              #진척도
        PROCSRQST.ProcsRequest,     #요청내용
            PROCSRST.ProcsResult,       #처리내용
        CS.Customer,                #고객사
        ASTD.Standard,              #표준본적용
        SL.Solution,                #솔루션
        PRJ_VERSION.name as ProjectName,        #프로젝트
        ISS_CATEGORY.name as Cateogry           #카테고리
FROM bitnami_redmine.issues as ISS
    #--숫자값으로 대체된 값에 대한 치환 / 필수값은 INNER JOIN , 빈값이있을수있는값은 LEFT OUTER JOIN
       INNER JOIN #업무유형 / 셀랙 안하고 불러올시 0.3초 느림
            (SELECT id, name as PrjType
                from bitnami_redmine.projects) as PRJ
            ON ISS.project_id = PRJ.id

        INNER JOIN #유형
            (SELECT id, name as TrkType
                from bitnami_redmine.trackers) as TRKR
            ON ISS.tracker_id = TRKR.id

        INNER JOIN #상태
            (SELECT id, name as StatsType
                from bitnami_redmine.issue_statuses) as STATS
            ON ISS.status_id = STATS.id

        INNER JOIN #우선순위
            bitnami_redmine.enumerations as PRIO
            ON ISS.priority_id = PRIO.id

        INNER JOIN #보고자
            bitnami_redmine.users as USER_AUTHOR
            ON ISS.author_id = USER_AUTHOR.id

        LEFT OUTER JOIN #담당자
            bitnami_redmine.users as USER_ASSIGNED
            ON ISS.assigned_to_id = USER_ASSIGNED.id

        LEFT OUTER JOIN #프로젝트
            bitnami_redmine.versions as PRJ_VERSION
            ON ISS.fixed_version_id = PRJ_VERSION.id
        LEFT OUTER JOIN #범주
            bitnami_redmine.issue_categories as ISS_CATEGORY
            ON ISS.category_id = ISS_CATEGORY.id
    #--기본 issue테이블에 존재하지않고 custom_values테이블에 저장된값들을 불러오기위한
    #--OuterJoin '값이 null이거나 해당 커스텀필드가 없는 이슈도있기때문에 outer로')
       LEFT OUTER JOIN #요청내용
            (SELECT custom_values.customized_id AS ID, custom_values.value AS ProcsRequest
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 3) AS PROCSRQST
              ON (ISS.id = PROCSRQST.id)

       LEFT OUTER JOIN #처리내용
            (SELECT custom_values.customized_id AS ID, custom_values.value AS ProcsResult
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 30) AS PROCSRST
              ON (ISS.id = PROCSRST.id)

       LEFT OUTER JOIN #처리예정시한
            (SELECT custom_values.customized_id AS ID, custom_values.value AS ProcsDate
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 24) AS PROCS
              ON (ISS.id = PROCS.id)

       LEFT OUTER JOIN #적용예정시한
            (SELECT custom_values.customized_id AS ID, custom_values.value AS EstDate
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 25) AS ESTD
              ON (ISS.id = ESTD.id)

       LEFT OUTER JOIN #고객사
            (SELECT custom_values.customized_id AS ID, custom_values.value AS Customer
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 9) AS CS
              ON (ISS.id = CS.id)

       LEFT OUTER JOIN #이슈확인자
            (SELECT custom_values.customized_id AS ID, custom_values.value AS IssueCheck, concat(USER_ASSIGNED.lastname, USER_ASSIGNED.firstname) as name
                FROM bitnami_redmine.custom_values AS custom_values
                left outer JOIN #담당자
                bitnami_redmine.users as USER_ASSIGNED
                ON custom_values.value = USER_ASSIGNED.id
                WHERE custom_values.custom_field_id = 45) AS IC
              ON (ISS.id = IC.id)

       LEFT OUTER JOIN #표준본적용여부
            (SELECT custom_values.customized_id AS ID, custom_values.value AS Solution
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 29) AS SL
              ON (ISS.id = SL.id)

       LEFT OUTER JOIN #솔루션 적용여부
           (SELECT custom_values.customized_id AS ID, custom_values.value AS Standard
              FROM bitnami_redmine.custom_values custom_values
             WHERE custom_values.custom_field_id = 46) AS ASTD
              ON (ISS.id = ASTD.id)


 WHERE #--해당이슈의 project_id를 체크하여 우리팀 이슈인지 확인)
       (ISS.project_id IN
          #--서브쿼리 : 우리팀 이슈를 판단할 ProjectID 가져오기 --
           (SELECT bitnami_redmine.projects.id
              FROM bitnami_redmine.projects
             WHERE    (bitnami_redmine.projects.parent_id = 48)
                   OR                                              #2016 nLM하위 메뉴들
                     (bitnami_redmine.projects.id = 48)
                   OR                                                #2016*(nLM자체)
                     (bitnami_redmine.projects.id = 24)
                   OR                                                #2016*(nLM자체)
                     (bitnami_redmine.projects.parent_id = 49))            #nOM
            #--eof 서브쿼리--
       )
order by Assigned asc, UpdateDate desc, ISS.id desc;
'''


def ConnectDB():
    db = MySQLdb.connect(host = '10.120.1.253', user = 'bitnami', passwd='488f050f8a', db='bitnami_redmine')
    db.query("set character_set_connection = utf8;")
    db.query("set character_set_server = utf8;")
    db.query("set character_set_client = utf8;")
    db.query("set character_set_results = utf8;")
    db.query("set character_set_database = utf8;")
    cursor = db.cursor()
    cursor.execute(c_query.encode('utf8'))
    data = cursor.fetchall()
    #final_data = list(data)
    db.close()
    return data#final_data
