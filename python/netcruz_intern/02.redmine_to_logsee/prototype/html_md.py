# -*- coding: utf-8 -*-
# html_md.py
def makeTag(tag,prop=''):
    if prop == '':
        f_tag = '<' + tag + '>'
    else:f_tag = '<' + tag + ' '+ prop +'>'
    return f_tag

def makeHTML(html_name,header,data,link1,link2):
    f = open(html_name,'w')
    bg_color = ('bgcolor=', ('"#eee6ff"', '"#ffffff"', '"#00e6ac"'))
    html = '''
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table>
'''
    f.write(html)
    f.write( makeTag('tr', bg_color[0]+bg_color[1][2]) + '\n')
    for idx, value in enumerate(header):
        f.write('<th>%s</th>\n' % value)
    f.write('</tr>\n')

    for idx, value in enumerate(data):
        f.write(makeTag('tr', bg_color[0]+bg_color[1][idx%2]) + '\n')
        link = ''
        for idx2, value2 in enumerate(value):
            if value2==None:
                value2 = ''
            f.write('<td>\n')
            if idx2 == header.index(link1):
                link = 'http://redmine.netcruz.co.kr/redmine/issues/'+value2
                f.write(makeTag('a','href='+link) + value2 + '</a>')
            elif idx2 == header.index(link2):
                f.write(makeTag('a','href='+link) + value2 + '</a>')
            else:
                f.write(value2)
            f.write('</td>')
        f.write('</tr>\n')
    f.write('</table></body></html>')
    f.close()
