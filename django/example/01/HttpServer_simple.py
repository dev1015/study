#-*- coding:utf-8 -*-
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.wfile.write("hello world")

if __name__ == '__main__':
    server = HTTPServer(('',5509), MyHandler)
    print 'Started WebSErver on port 5509'
    print 'press ^c to quit WebServer'
    server.serve_forever()
