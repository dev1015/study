#Main_program.py
# -*- coding: utf-8 -*-
import os
import pickle
from datetime import datetime
#Custom_module
import DB_Shooter
import DB_Connector

db_comparison = False

#헤더생성
f = open('header.txt', 'r')
header = f.readlines()
f.close
for idx, value in enumerate(header):
    header[idx] = header[idx].rstrip('\n')
    header[idx] = header[idx].split(',')

#가장최근 덤프된 파일확인
old_file = os.listdir('./dump/')
if len(old_file) == 0:
    db_comparison = False
else:
    db_comparison = True
    old_file.sort(reverse=True) #가장 최근 파일 가져오기
    old_path = './dump/%s' % old_file[0]
    print '@exist  :',old_path
    f = open(old_path,'r')
    old_db = pickle.load(f) #이전 DB 가져오기
    f.close()

#Redmine DB연결
new_db = DB_Connector.ConnectDB()

#DB분석
#이전 DB ID값 리스트
old_index = {} #이전DB 인덱스{ID : 리스트인덱스값}
new_index = {} #새DB 인덱스
send_index = []
#새로들어온 DB결과에대한 인덱스 작성
for idx, value in enumerate(new_db):
    new_index[value[0]] = idx #{issueid:DB리스트에 기록된 행번호}
#DB를 비교할수있을때
if db_comparison == True:
    #이전DB에대한 인덱스 생성
    for idx, value in enumerate(old_db):
        old_index[value[0]] = idx
    #새DB의 내용을 이전DB와 비교(인덱스 이용)
    for trash_idx,id_key in enumerate(new_index):
        #새DB의 이슈넘버(id_key)가 이전DB에도 존재하는가
        if (id_key in old_index) == True:
            if new_db[new_index[id_key]] != old_db[old_index[id_key]]:
                #send
                DB_Shooter.shootLOG(header[0],new_db[new_index[id_key]],514)
                print 'DB Update) id_%s' % id_key
                #print '!!#',new_db[new_index[id_kddey]]
                #print '!!@',old_db[old_index[id_key]]
            #내용이 같을경우 아무것도하지않음
            else:
                print '@DB equal = %s' % id_key
                pass
        #새로추가된 내용이라면 추가전송
        else:
            DB_Shooter.shootLOG(header[0],new_db[new_index[id_key]],514)
            print 'DB Added) id_%s' % id_key
#이전DB(덤프파일)이없을시(~최초실행시) 새로 가져온 DB내용 모두 전송
else:
    DB_Shooter.shootLOG(header[0],new_db,514)
    print 'Dump does not exist'
    print 'send DB(ALL DATA)Complete'
#새로 읽어온 DB덤프
filename = datetime.today().strftime("%Y-%m-%d_%H.%M.%S")
dump_path = './dump/DB_%s.dump' % filename
print '@Created :', dump_path
f = open(dump_path,'w')
pickle.dump(new_db,f)
f.close()
if db_comparison == True:
    #이전DB와 비교완료후에는 이전DB Dump를 삭제
    os.remove(old_path)
    print '@Remove : ',old_path
